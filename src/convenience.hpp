/// \file convenience.hpp
/// \brief templates used for convenience
#ifndef CONVENIENCE_H
#define CONVENIENCE_H

#include <algorithm>
#include <iterator>
#include <memory>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

/// \brief checks that the value is inside the specified domain
/// @param val [in] value to test
/// @param min [in] minimum value, inclusive
/// @param max [in] maximum value, inclusive
/// @param message [in] message to use when raising an exception
/// @throws std::domain_error if outside
template <typename T>
void check_domain(T val, T min, T max, const std::string& message) {
  if (val < min) {
    throw std::domain_error(message + ": " + std::to_string(val));
  }
  if (val > max) {
    throw std::domain_error(message + ": " + std::to_string(val));
  }
}

/// \brief Creates a new vector that contains all the elements from the input
/// transformed with the given operation.
/// @param c [in] STL-like container
/// @param f [in] unary operator (can be a lambda)
/// @return a vector v, where v[i] = f(c[i])
template <typename It, typename Op>
auto transformed(const It& c, const Op& f)
    -> std::vector<decltype(f(c.at(0)))> {
  std::vector<decltype(f(c.at(0)))> out(c.size());
  std::transform(c.cbegin(), c.cend(), begin(out), f);
  return out;
}

/// \brief Return a set that is the union of the given sets
/// The original sets are not modified.
/// @return new set
template <typename T, typename Comp, typename Alloc>
std::set<T, Comp, Alloc> build_set_union(const std::set<T, Comp, Alloc>& a,
                                         const std::set<T, Comp, Alloc>& b) {
  std::set<T, Comp, Alloc> out;
  std::copy(a.cbegin(), a.cend(), std::inserter(out, begin(out)));
  std::copy(b.cbegin(), b.cend(), std::inserter(out, begin(out)));
  return out;
}

/// \brief Either T or empty. The default constructor builds an empty value.
/// (needed because std::optional would be C++17)
template <typename T>
using option = std::shared_ptr<T>;

/// Build an option<T> with specified contents
template <typename T>
constexpr option<T> make_option(const T& v) {
  return std::make_shared<T>(v);
}

template <typename T>
bool is_none(option<T> v) {
  return v == nullptr;
}

/// \brief Checks whether the two options are identical
/// @note
/// You cannot simply dereference both values and check for equality because
/// either value may contain the null value.
/// @param a [in]
/// @param b [in]
/// @return true if both a and b are null, or both a and b are non-null, and
/// dereferencing yields equivalent values
template <typename T>
bool is_same(const option<T>& a, const option<T>& b) noexcept {
  // both defined
  if (a && b) {
    return *a == *b;
  }
  // or both null
  return a == nullptr && b == nullptr;
}

/// \brief compare two shared pointers by dereferencing them
/// null pointers compare less than non-null pointers
template <typename T>
struct deref_less {
  bool operator()(const std::shared_ptr<T>& a,
                  const std::shared_ptr<T>& b) const {
    if (a != nullptr) {
      if (b != nullptr) {
        return *a < *b;
      }
      return false;  // non-null < null => false
    }
    return b != nullptr;  // null < non-null => true
  }
};

#endif
