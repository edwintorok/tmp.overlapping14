/// \file rectangle.hpp
/// \brief Basic types (rectangles, points).
/// All operations in this header have O(1) time and space complexity.

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "convenience.hpp"

#include <cstdint>
#include <memory>

/// \brief according to RFC7159
/// -2^53+1
constexpr int64_t json_min = -(1LL << 53) + 1;

/// \brief according to RFC7159
/// 2^53-1
constexpr int64_t json_max = (1LL << 53) - 1;

/*! \brief type alias for point coordinates */
using coordinate = int64_t;

/*! type alias for width/height */
using distance = int64_t;

/// \brief wrapper for x, y coordinates
/// The struct is immutable, all operations return new points.
struct Point {
  /*! \brief must construct with explicit x and y */
  Point() = delete;

  /*! \brief Builds a point.
    Negative coordinates are valid. */
  Point(coordinate x, coordinate y) noexcept : x(x), y(y) {}

  /*! \brief column coordinate */
  const coordinate x;

  /*! \brief row coordinate */
  const coordinate y;
};

/*! \brief compares two Points */
bool operator==(Point a, Point b) noexcept;

/*! writes a string representation of points on specified stream */
std::ostream& operator<<(std::ostream& os, const Point& p);

/// \brief wrapper for width and height
/// The struct is immutable, all operations return new sizes.
struct Size {
  /*! \brief must construct with explicit w and h */
  Size() = delete;

  /*! Builds a size.
    Negative sizes are accepted */
  Size(distance w, distance h) noexcept : w{w}, h{h} {}

  /*! \brief checks whether the size is valid (positive width and height) */
  explicit operator bool() const noexcept { return w > 0 && h > 0; }

  /*! \brief width */
  const distance w;

  /*! \brief height */
  const distance h;
};

/*! compares Sizes for equality */
bool operator==(Size a, Size b) noexcept;

/*! writes a string representation of size on the specified stream */
std::ostream& operator<<(std::ostream& os, const Size& s);

/// \brief A rectangle
/// Invariant: rectangle width and height must be positive.
/// Only valid rectangles can be constructed.
/// The Myrectangle is immutable, all operations return new Myrectangles.
/// @note: the "Myrectangle" name clashes with something in MSVC
class Myrectangle {
 public:
  /// \brief Builds a Myrectangle with specified top-left corner point and size
  /// Negative corner coordinates are accepted.
  /// @throws std::domain_error if width <= 0 or height <= 0
  Myrectangle(Point p, Size s) : top_left{p}, size{s} {
    check_domain(s.w, int64_t{1}, INT64_MAX, "Width must be positive");
    check_domain(s.h, int64_t{1}, INT64_MAX, "Height must be positive");
  }

  /// \brief bottom-right corner
  /// The bottom-right corner point that is still part of this rectangle.
  /// If the rectangle has size {1, 1} then this is the same as its top-left
  /// corner
  Point bot_right() const noexcept;

  /// \brief Calculates the intersection of two rectangles
  /// The intersection rectangle contains all points that belong to both
  /// rectangles.
  /// @param that [in] rectangle to intersect with
  /// @return either an empty option<> if the two rectangles do not intersect,
  /// or an option<Myrectangle> that represents the intersection.
  option<Myrectangle> intersect(const Myrectangle& that) const noexcept;

  /*! \brief the top left corner specified during construction */
  const Point top_left;

  /*! \brief rectangle size specified during construction */
  const Size size;
};

/*! \brief compares Myrectangles for equality */
bool operator==(Myrectangle a, Myrectangle b) noexcept;

/*! Writes a string representation of the rectangle on the specified stream. */
std::ostream& operator<<(std::ostream& os, const Myrectangle& r);

/*! \brief Myrectangle IDs (positive numbers) */
using id = unsigned;

//// \brief A rectangle with an ID
using Myrectangleid = std::pair<id, Myrectangle>;

/// \brief smart pointer to rectangle
/// Intersections will need to retain references to rectangles, and to ensure
/// proper lifetime management a shared pointer is used.
/// It would suffice to retain just the rectangle IDs, however future changes
/// might require access to more information from the rectangle itself.
using PMyrectangle = std::shared_ptr<Myrectangleid>;

/// \brief compares two rectangles
/// Compares rectangles by id. Two rectangles with identical points, but
/// different id are nevertheless different
bool operator<(const Myrectangleid& a, const Myrectangleid& b) noexcept;
bool operator<(const PMyrectangle& a, const PMyrectangle& b) noexcept;

/// \brief Builds a new smart pointer for the given rectangle.
/// The assigned id should be unique.
PMyrectangle make_prectangle(id id, Myrectangle a);
#endif
