/// \file intersections.hpp
/// \brief Calculates intersection sets
#ifndef INTERSECTIONS_H

#define INTERSECTIONS_H
#include "rectangle.hpp"

#include <set>

/// \brief a set of rectangles
/// @note
/// could use unordered_set and define std::hash specializations
/// if the order in the output doesn't matter
using MyrectangleSet = std::set<PMyrectangle, deref_less<Myrectangleid>>;

/// \brief An intersection between two or more rectangles
/// Retains references to all rectangles that are part of the intersection.
/// Stores the result of the intersection.
/// An intersection can be empty.
/// Intersections are immutable, all operations construct new intersections.
class Intersection {
 public:
  /*! Calculates the intersection of two rectangles. */
  Intersection(PMyrectangle a, PMyrectangle b);

  /*! Calculate the intersection of two existing intersections */
  Intersection(const Intersection& a, const Intersection& b);

  /// \brief checks whether the intersection is empty
  /// An intersection is empty if the rectangle is empty.
  /// @note:
  /// an empty intersection nevertheless retains which rectangles it resulted
  /// from
  bool is_empty() const noexcept;

  /*! \brief the rectangle resulting from the intersection of rectangles */
  const option<Myrectangle> rect;

  /*! which unique rectangles have participated in this intersection */
  const MyrectangleSet rectangles;
};

/// \brief compares two intersections
/// Two intersections are considered equal if the unique rectangles that were
/// used to construct it are the same.
/// Two empty intersections can be considered different.
bool operator<(const Intersection& a, const Intersection& b);

/*! \brief Writes a string representation of the intersection on the stream */
std::ostream& operator<<(std::ostream& os, const Intersection& i);

/*! a smart pointer to an intersection */
using PIntersection = std::shared_ptr<Intersection>;

/*! set of unique intersections */
using PIntersectionUnique = std::set<PIntersection, deref_less<Intersection>>;

/// \brief Calculates the powerset of all intersections
/// Given a set of rectangles finds each intersecting pair, each intersecting
/// triple, etc.
/// only one of A ∩ B and B∩A is reported, i.e. the rectangles participating
/// in each intersection are unique.
/// This class is mutable.
class IntersectionSet {
 public:
  /*! construct an empty set */
  IntersectionSet() = default;

  /// \brief construct the intersection of all rectangles.
  /// Conceptually equivalent to constructing the empty set and inserting all
  /// rectangles, except it may use a more efficient algorithm.
  explicit IntersectionSet(const std::vector<PMyrectangle>& input);

  /// \brief inserts the specified rectangle in the set.
  /// May insert new intersections. Invalidates iterators.
  void insert(const PMyrectangle& rect);

  /*! \brief constant STL-like begin iterator */
  auto cbegin() const noexcept { return intersections.cbegin(); }

  /*! \brief constant STL-like end iterator */
  auto cend() const noexcept { return intersections.cend(); }

  /*! \brief constant STL-like begin iterator */
  auto begin() const noexcept { return cbegin(); }

  /*! \brief constant STL-like end iterator */
  auto end() const noexcept { return cend(); }

  /*! \brief number of intersections */
  auto size() const noexcept { return intersections.size(); }

 private:
  void insert(const PIntersection& inter);

  MyrectangleSet rectangles;
  PIntersectionUnique intersections;
  std::vector<PIntersectionUnique> intersections_by_rect_id;
};

/*! \brief Writes a string representation of the intersection sets */
std::ostream& operator<<(std::ostream& os, const IntersectionSet& iset);
#endif
