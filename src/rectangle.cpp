#include "rectangle.hpp"
#include "convenience.hpp"

#include <algorithm>
#include <cassert>
#include <memory>
#include <ostream>

bool operator==(Point a, Point b) noexcept { return a.x == b.x && a.y == b.y; }
bool operator==(Size a, Size b) noexcept { return a.w == b.w && a.h == b.h; }
bool operator==(Myrectangle a, Myrectangle b) noexcept {
  return a.top_left == b.top_left && a.size == b.size;
}

std::ostream& operator<<(std::ostream& os, const Point& p) {
  os << "(" << p.x << "," << p.y << ")";
  return os;
}

std::ostream& operator<<(std::ostream& os, const Size& s) {
  return os << "w=" << s.w << ", h=" << s.h;
}

std::ostream& operator<<(std::ostream& os, const Myrectangle& r) {
  return os << "at " << r.top_left << ", " << r.size;
}

Point Myrectangle::bot_right() const noexcept {
  return Point{top_left.x + size.w - 1, top_left.y + size.h - 1};
}

option<Myrectangle> Myrectangle::intersect(const Myrectangle& that) const
    noexcept {
  using std::min;
  using std::max;
  Point new_top_left{max(top_left.x, that.top_left.x),
                     max(top_left.y, that.top_left.y)};

  Point this_botright = bot_right();
  Point that_botright = that.bot_right();
  Point new_bottom_right{min(this_botright.x, that_botright.x),
                         min(this_botright.y, that_botright.y)};

  Size size{new_bottom_right.x - new_top_left.x + 1,
            new_bottom_right.y - new_top_left.y + 1};

  if (size) {
    return make_option(Myrectangle{new_top_left, size});
  }
  return {};
}

bool operator<(const Myrectangleid& a, const Myrectangleid& b) noexcept {
  return a.first < b.first;
}

bool operator<(const PMyrectangle& a, const PMyrectangle& b) noexcept {
  return deref_less<Myrectangleid>()(a, b);
}

PMyrectangle make_prectangle(id id, Myrectangle a) {
  return std::make_shared<std::pair<unsigned, Myrectangle>>(
      std::make_pair(id, a));
}
