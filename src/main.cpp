#include "mainio.hpp"

#include <iostream>
#include <stdexcept>
#include <vector>

int main(int argc, char* argv[]) {
  /// don't use raw pointers
  std::vector<char*> args(argv, argv + argc);
  if (args.size() != 2) {
    std::cerr << "Usage: " << args.at(0) << " <input.json>\n";
    return 1;
  }
  std::string filename{args.at(1)};
  try {
    process(std::cout, filename);
  } catch (const std::logic_error& e) {
    std::cerr << "Error parsing JSON '" << filename << "': " << e.what()
              << "\n";
    return 2;
  } catch (const std::exception& e) {
    std::cerr << "Fatal error parsing JSON '" << filename << "': " << e.what()
              << "\n";
    return 3;
  }
  return 0;
}
