#include "intersections.hpp"
#include <algorithm>
#include <cassert>
#include <memory>
#include <ostream>
#include <stack>

namespace {
option<Myrectangle> intersect(const option<Myrectangle>& a,
                              const option<Myrectangle>& b) {
  if (a && b) {
    return a->intersect(*b);
  }
  return option<Myrectangle>{};
}

option<Myrectangle> intersect(const PMyrectangle& a, const PMyrectangle& b) {
  if (a && b) {
    return a->second.intersect(b->second);
  }

  return option<Myrectangle>{};
}
}  // namespace

Intersection::Intersection(PMyrectangle a, PMyrectangle b)
    : rect{intersect(a, b)}, rectangles{a, b} {}

Intersection::Intersection(const Intersection& a, const Intersection& b)
    : rect{intersect(a.rect, b.rect)},
      rectangles{build_set_union(a.rectangles, b.rectangles)} {}

bool operator<(const Intersection& a, const Intersection& b) {
  auto n1 = a.rectangles.size();
  auto n2 = b.rectangles.size();
  return n1 == n2 ? a.rectangles < b.rectangles : n1 < n2;
}

bool Intersection::is_empty() const noexcept { return !rect; }

std::ostream& operator<<(std::ostream& os, const Intersection& i) {
  if (i.rect) {
    os << "Between rectangle ";
    std::vector<PMyrectangle> rectangles{i.rectangles.cbegin(),
                                         i.rectangles.cend()};
    std::sort(std::begin(rectangles), std::end(rectangles));
    int j = rectangles.size();
    for (const auto& r : rectangles) {
      if (r) {
        os << r->first;
        switch (--j) {
          case 0:
            break;
          case 1:
            os << " and ";
            break;
          default:
            os << ", ";
            break;
        }
      }
    }
    return os << " " << *i.rect << ".";
  }
  return os;
}

IntersectionSet::IntersectionSet(const std::vector<PMyrectangle>& input)
    : rectangles{}, intersections{} {
  if (!input.empty()) {
    auto last = std::max_element(input.cbegin(), input.cend());
    unsigned id = (*last)->first + 1;
    intersections_by_rect_id.resize(id);
    for (const auto& r : input) {
      insert(r);
    }
  }
}

void IntersectionSet::insert(const PMyrectangle& rect) {
  if (rect && rectangles.count(rect) == 0) {
    std::set<Intersection> more;
    auto& rect_intersections = intersections_by_rect_id.at(rect->first);
    for (const auto& a : rectangles) {
      PIntersection p{std::make_shared<Intersection>(Intersection{a, rect})};
      rect_intersections.insert(p);
      insert(p);
    }
    rectangles.insert(rect);
  }
}

void IntersectionSet::insert(const PIntersection& inter) {
  std::deque<PIntersection> work{inter};
  while (!work.empty()) {
    auto inter = work.front();
    work.pop_front();
    if (!inter->is_empty() && intersections.count(inter) == 0) {
      for (const auto& r : inter->rectangles) {
        auto candidates = intersections_by_rect_id.at(r->first);
        std::transform(
            candidates.cbegin(), candidates.cend(), std::back_inserter(work),
            [&inter](const auto& i) {
              return std::make_shared<Intersection>(Intersection{*i, *inter});
            });
        candidates.insert(inter);
      }
      intersections.insert(inter);
    }
  }
}

std::ostream& operator<<(std::ostream& os, const IntersectionSet& iset) {
  for (const auto& intersection : iset) {
    if (intersection) {
      os << "\t" << *intersection << "\n";
    }
  }
  return os;
}
