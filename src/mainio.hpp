/// \file mainio.hpp
/// \brief I/O entrypoints
#ifndef MAINIO_H
#define MAINIO_H

#include "rectangle.hpp"

#include <iosfwd>
#include <vector>

/// \brief Parses the JSON from the specified stream
/// The JSON must have at least a 'rect' field in its root object,
/// that contains an array of JSON objects specifying rectangles.
/// Myrectangles are specified by their 'x', 'y' topleft coordinates,
/// 'w' and 'h' size.
/// Integers are represented as 64-bit to cover the full range of integers
/// encodable in JSON (which is [-2^53+1,2^53-1]).
/// Extra fields present in the JSON result show a warning on stderr.
/// Missing fields throw errors
/// @param is [in] a valid input stream containing a UTF-8 encoded JSON
/// @return vector of rectangles
/// @throws std::logic_error subclass if the JSON is invalid: either because of
/// an underlying I/O error, a JSON syntax error, missing fields, or wrong field
/// types.
std::vector<PMyrectangle> parse_json(std::istream& is);

/// \brief Processes the specified input file
/// Opens the specified file, parses it with parse_json, builds an
/// IntersectionSet, and outputs it to the specified stream
/// @param os output stream
/// @param filename [in] path of input JSON
/// @param max_rectangles [in] optional parameter limiting number of rectangles
/// @throws std::logic_error @see parse_json
void process(std::ostream& os, const std::string& filename,
             unsigned max_rectangles = 1000);

#endif
