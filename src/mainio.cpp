#include "mainio.hpp"
#include "intersections.hpp"

#include <fstream>
#include <iostream>
#include <json.hpp>

using nlohmann::json;

int64_t get(const json& j, const std::string& field) {
  const auto fieldval = j.at(field);
  if (!fieldval.is_number_integer()) {
    throw std::invalid_argument("integer expected for field " + field +
                                ", got: " + fieldval.dump());
  }
  int64_t v{fieldval};
  if (v < json_min || v > json_max) {
    std::cerr << "JSON number " << v << " is not portable: outside ["
              << json_min << ", " << json_max << "]";
  }
  return v;
}

std::vector<PMyrectangle> parse_json(std::istream& is) {
  json j;
  is >> j;

  unsigned n = 1;
  if (!j.is_object()) {
    throw std::invalid_argument("expected a JSON object as root");
  }
  const auto& rects = j.at("rects");
  if (!rects.is_array()) {
    throw std::invalid_argument(
        "'rects' is expected to be a JSON array, got: " + rects.dump());
  }
  if (j.size() != 1) {
    std::cerr << "Warning: Unknown JSON fields (other than 'rects') ignored\n";
  }
  return transformed(rects, [&n](auto json) {
    if (!json.is_object()) {
      throw std::invalid_argument("expected JSON object, got: " + json.dump());
    }
    auto r =
        make_prectangle(n++, Myrectangle{Point{get(json, "x"), get(json, "y")},
                                         Size{get(json, "w"), get(json, "h")}});
    if (json.size() != 4) {
      std::cerr << "Warning: Unknown JSON fields ignored in: " << json.dump()
                << "\n";
    }
    assert(r != nullptr);  // make_shared throws bad_alloc instead
    return r;
  });
}

auto load(const std::string& file) {
  std::ifstream f(file);
  return parse_json(f);
}

void process(std::ostream& os, const std::string& filename,
             const unsigned max_rectangles) {
  auto rectangles = load(filename);
  if (rectangles.size() > max_rectangles) {
    rectangles.resize(max_rectangles);
  }
  os << "Input:\n";
  for (const auto& rect : rectangles) {
    if (rect != nullptr) {
      os << "\t" << rect->first << ": Myrectangle " << rect->second << ".\n";
    }
  }
  os << "\n";
  os << "Intersections\n";
  os << IntersectionSet{rectangles};
}
