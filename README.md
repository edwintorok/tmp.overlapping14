[![Build Status](https://travis-ci.org/edwintorok/overlapping14.svg?branch=master)](https://travis-ci.org/edwintorok/overlapping14)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/edwintorok/overlapping14?branch=master&svg=true)](https://ci.appveyor.com/project/edwintorok/overlapping14)
[![Coverage Status](https://coveralls.io/repos/github/edwintorok/overlapping14/badge.svg?branch=master)](https://coveralls.io/github/edwintorok/overlapping14?branch=master)

# Building

 Prerequisites:

 * C++14 compliant compiler (Clang 3.5+, GCC 5+, Visual Studio 2015)
 
 Tested on:
 Ubuntu 16.04 LTS (CMake 3.5.1, GCC 5.4.0), Ubuntu 16.10 (CMake 3.5.2, GCC 6.2.0/Clang 3.8.1), Fedora 25 (CMake 3.6.2, GCC 6.3.1), Alpine 3.5 (CMake 3.6.3, GCC 6.2.1, Musl 1.1.15), OS X 10.11.6 (XCode 7.3.1, AppleClang 7.3.0.7030031), Windows (Visual Studio 14 2015, MSVC 19.0.24215.1)
 
## Quick start (on Linux)

If you are using Ubuntu 16.10:
```sh
$ sudo apt-get update -y && sudo apt-get install -y cmake build-essential g++ git
$ git clone https://github.com/edwintorok/overlapping14 && cd overlapping14 && git submodule update --init --recursive
$ mkdir obj && cd obj && cmake .. && make -j8 -O
$ make test ARGS=-V
```

You should now have a `./Intersecting` executable in the current directory,
run it with the filename of the input JSON you want to test it with: the output will be shown on stdout,
errors on stderr.

For more detailed instructions, and other OSs read below.

## Cloning

If you have cloned the repository you need to update submodules:
```sh
git submodule update --init --recursive
```

## Building with CMake (recommended)

 Requires CMake 3.2+
 
 On *nix it is recommended to build in a subdir:
 ```sh
 $ mkdir obj && cd obj && cmake .. && make -j8 . && make test ARGS=-V
 ```

 On Windows:
 ```cmd
 cmake -G "Visual Studio 14 2015" -DCMAKE_BUILD_TYPE=RelWithDebinfo
 cmake --build . --config RelWithDebinfo
 ctest -C RelWithDebinfo
 ```

## Building without CMake

If you have clang 3.5+ and Libc++ installed then you can use the provided
`GNUmakefile.clang`, for example on Ubuntu 14.04:

```sh
$ sudo apt-get install -y build-essential clang-3.5 libc++-dev
$ make -f GNUmakefile.clang CXX=clang++-3.5 -j4
```

# Input JSON

A JSON object with a `rects` field at its root, containing a JSON array of rectangles.
Each rectangle is a JSON object containing `x`, `y`, `w`, and `h` integer fields.
`x` and `y` may be negative, but `w` and `h` must be positive.

Each rectangle is assigned a unique ID counting from 1 automatically.
Two rectangles with same coordinates and size are nevertheless considered distinct.

The program will exit with an error if the input JSON is invalid and doesn't contain rectangles.
Additional fields beyond those specified above will print a warning, but execution will continue.

# Output

The rectangles read from JSON, followed by a list of all unique intersections between two or more rectangles.
Two intersections are considered the same if they were constructed from the same unique set of rectangles.
Intersection containing rectangles `{1, 2}` and `{2, 1}` are the same, but intersections
`{1, 2, 3}` and `{1, 2}` are different.

# Testing

The built-in test can be run with `make test` or `make test ARGS=-V` (for verbose output).

# Linting/static analysis

Various linters and static analysis tools can be optionally run with `ctest -j8 -V` from the source directory.
Requires `clang`, `clang-analyzer`, `clang-tidy` and `valgrind` to be installed.

# 3rdparty libraries used

The main executable `Intersecting` requires only `nlohmann/json` header-only library, which is included in `3rdparty/`.
The tests also require `rapidcheck` and `Boost.Test`, which are also included.

# Documentation

If you have `doxygen` installed, you can build the documentation by simply running doxygen from the root of the checkout.
