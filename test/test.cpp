#include "convenience.hpp"
#include "intersections.hpp"
#include "mainio.hpp"
#include "rectangle.hpp"

#define BOOST_TEST_MODULE OverlapTest
// order matters here, boost must be included first!
#include <boost/test/included/unit_test.hpp>
#include <rapidcheck.h>
#include <rapidcheck/boost_test.h>

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iterator>
#include <sstream>
#include <type_traits>
#include <vector>

namespace rc {

static inline auto gen_number() { return gen::inRange(json_min, json_max); }

template <>
struct Arbitrary<Point> {
  static Gen<Point> arbitrary() {
    return gen::construct<Point>(gen_number(), gen_number());
  }
};

template <>
struct Arbitrary<Size> {
  static Gen<Size> arbitrary() {
    return gen::construct<Size>(gen_number(), gen_number());
  }
};

template <>
struct Arbitrary<Myrectangle> {
  static Gen<Myrectangle> arbitrary() {
    return gen::construct<Myrectangle>(
        gen::arbitrary<Point>(),
        gen::construct<Size>(gen::inRange(int64_t{1}, json_max),
                             gen::inRange(int64_t{1}, json_max)));
  }
};
}  // namespace rc

namespace {
static_assert(!std::is_default_constructible<Point>::value, "no Point()");
static_assert(!std::is_default_constructible<Size>::value, "no Size()");
static_assert(!std::is_default_constructible<Myrectangle>::value,
              "no Myrectangle()");
static_assert(std::is_copy_constructible<Myrectangle>::value,
              "Myrectangle copy");
static_assert(std::is_nothrow_copy_constructible<Myrectangle>::value,
              "Myrectangle copy nothrow");

static_assert(std::is_nothrow_default_constructible<option<int>>::value,
              "option null nothrow");
static_assert(std::is_copy_constructible<option<int>>::value, "option copy");

static_assert(!std::is_default_constructible<Intersection>::value,
              "no Intersection()");
static_assert(std::is_copy_constructible<Intersection>::value,
              "Intersection() copy");

static_assert(std::is_default_constructible<IntersectionSet>::value,
              "IntersectionSet()");

BOOST_AUTO_TEST_CASE(constructors) {
  BOOST_TEST(!option<int>{});
  auto o0 = make_option<int>(0);
  auto o1 = make_option<int>(1);
  auto o0_copy = o0;
  auto o1_copy = o1;
  BOOST_TEST(o0);
  BOOST_TEST(o1);
  BOOST_TEST(*o0 == 0);
  BOOST_TEST(*o1 == 1);
  BOOST_TEST(*o0 == *o0_copy);
  BOOST_TEST(*o1 == *o1_copy);

  auto x = 4;
  auto y = 5;
  Point p{x, y};
  BOOST_TEST(p.x == x);
  BOOST_TEST(p.y == y);

  auto w = 6;
  auto h = 7;
  Size s{w, h};
  BOOST_TEST(s.w == w);
  BOOST_TEST(s.h == h);

  Myrectangle r{p, s};
  BOOST_TEST(r.top_left == p);
  BOOST_TEST(r.size == s);
  Point expected{9, 11};
  BOOST_TEST(r.bot_right() == expected);
  BOOST_TEST(!(expected == p));
}

RC_BOOST_PROP(domain, (int start, unsigned size)) {
  int end = start + size;
  RC_PRE(start <= end);
  RC_ASSERT_THROWS_AS(check_domain(start - 1, start, end, "< min"),
                      std::domain_error);
  RC_ASSERT_THROWS_AS(check_domain(end + 1, start, end, "> max"),
                      std::domain_error);
  check_domain(start, start, end, "must not throw");
  check_domain(end, start, end, "must not throw");
}

RC_BOOST_PROP(validMyrectangles, (Myrectangle r)) {
  RC_ASSERT(r == r);
  auto p1 = r.top_left;
  auto p2 = r.bot_right();
  Size s{p2.x - p1.x + 1, p2.y - p1.y + 1};
  RC_ASSERT(s == r.size);

  // intersection of rectangle with itself equals itself
  auto i = r.intersect(r);
  RC_ASSERT(i);
  RC_ASSERT(*i == r);
}

RC_BOOST_PROP(invalidMyrectangles, (Point p, Size s)) {
  RC_PRE(s.w <= 0 || s.h <= 0);
  RC_ASSERT_THROWS_AS((Myrectangle{p, s}), std::domain_error);
  RC_ASSERT_THROWS_AS((Myrectangle{p, Size{0, 0}}), std::domain_error);
}

RC_BOOST_PROP(intersectCommutes, (Myrectangle a, Myrectangle b)) {
  auto i1 = a.intersect(b);
  auto i2 = b.intersect(a);
  RC_ASSERT(is_same(i1, i2));
}

RC_BOOST_PROP(prectOps,
              (unsigned id1, Myrectangle a, unsigned id2, Myrectangle b)) {
  RC_PRE(!(a == b) && !(id1 == id2));

  auto p1 = make_prectangle(id1, a);
  auto p2 = make_prectangle(id2, a);
  deref_less<Myrectangleid> comp;
  if (id1 < id2)
    RC_ASSERT(comp(p1, p2));
  else
    RC_ASSERT(comp(p2, p1));
  RC_ASSERT(!comp(p1, p1));
  RC_ASSERT(!comp(p2, p2));

  auto p3 = make_prectangle(id1, a);
  RC_ASSERT(!comp(p1, p3) && !comp(p3, p1));

  MyrectangleSet rs;
  rs.insert(p1);
  rs.insert(p2);
  rs.insert(p3);
  RC_ASSERT(rs.size() == 2u);

  std::vector<PMyrectangle> v{p1, p2, p1, p2, p1};
  std::sort(std::begin(v), std::end(v));
  RC_ASSERT(std::is_sorted(v.cbegin(), v.cend()));
}

RC_BOOST_PROP(intersection, (Myrectangle a, Myrectangle b)) {
  PMyrectangle pa{make_prectangle(1, a)};
  PMyrectangle pb{make_prectangle(2, b)};
  Intersection inter{pa, pb};

  RC_CLASSIFY(inter.is_empty(), "no intersection");
  if (inter.is_empty()) {
    RC_ASSERT(is_none(inter.rect));
  } else {
    RC_ASSERT(!is_none(inter.rect));
    RC_ASSERT(inter.rectangles == MyrectangleSet({pa, pb}));
  }

  auto pi1 = std::make_shared<Intersection>(inter);
  auto pi2 = std::make_shared<Intersection>(inter);
  PIntersectionUnique pu;
  pu.insert(pi1);
  pu.insert(pi2);
  RC_ASSERT(pu.size() == 1u);
}

RC_BOOST_PROP(intersect_none, (Myrectangle a)) {
  PMyrectangle p{make_prectangle(1, a)};
  PMyrectangle pnull{};

  bool b1 = pnull < p;
  bool b2 = !(p < pnull);
  bool b3 = !(p < p);

  // don't put nulls into assert, they cannot be displayed
  RC_ASSERT(b1);
  RC_ASSERT(b2);
  RC_ASSERT(b3);

  Intersection i_full{p, p};
  RC_ASSERT(!i_full.is_empty() && *i_full.rect == a);

  Intersection i_full2{p, p};
  RC_ASSERT(!i_full2.is_empty() && *i_full.rect == a);

  Intersection i1{nullptr, nullptr};
  Intersection i2{nullptr, p};
  Intersection i3{p, nullptr};
  Intersection i4{i1, i2};
  Intersection i5{i3, i_full};
  Intersection i6{i_full, i3};
  RC_ASSERT(i1.is_empty());
  RC_ASSERT(i2.is_empty());
  RC_ASSERT(i3.is_empty());
  RC_ASSERT(i4.is_empty());
  RC_ASSERT(i5.is_empty());
  RC_ASSERT(i6.is_empty());

  std::stringstream ss;
  ss << i1;
}

const auto N = 100;
auto gen_small() { return *rc::gen::inRange(1, N); }
auto gen_small_point() { return Point{gen_small() - 1, gen_small() - 1}; }
auto gen_small_size() { return Size{gen_small(), gen_small()}; }
auto gen_small_rect() {
  return Myrectangle{gen_small_point(), gen_small_size()};
}

RC_BOOST_PROP(intersectVsNaive, ()) {
  auto a = gen_small_rect(), b = gen_small_rect();

  const auto max_x = std::max(a.bot_right().x, b.bot_right().x) + 1;
  const auto max_y = std::max(a.bot_right().y, b.bot_right().y) + 1;

  using Matrix = std::vector<int>;
  const coordinate shape = max_x * max_y;

  auto draw = [max_x, max_y](Matrix &matrix, Myrectangle r) {
    for (coordinate x = r.top_left.x; x <= r.bot_right().x; x++) {
      for (coordinate y = r.top_left.y; y <= r.bot_right().y; y++) {
        matrix.at(y * max_x + x)++;
      }
    }
  };

  // naive intersection method:
  // draw both rectangles and count intersecting points
  Matrix m1(shape), m2(shape);
  draw(m1, a);
  draw(m1, b);
  std::transform(std::begin(m1), std::end(m1), std::begin(m1), [](auto v) {
    // 0 = empty; 1 = non-intersecting part of a or b; 2 = intersection
    return v == 2 ? 1 : 0;
    // set 1 for intersection, 0 for no
  });

  // draw the intersection calculated by Myrectangle::intersect and check that
  // it is correct
  auto intersection = a.intersect(b);
  RC_CLASSIFY(is_none(intersection), "has intersection");
  if (intersection) draw(m2, *intersection);
  RC_ASSERT(m1 == m2);
}

auto prectangles(const std::vector<Myrectangle> &rects) {
  unsigned i = 1;
  return transformed(rects,
                     [&i](const auto &r) { return make_prectangle(i++, r); });
}

auto is_unique(const IntersectionSet &iset) {
  // check for uniqueness (even though the implementation currently uses as set)
  std::vector<Intersection> intersections;
  std::set<Intersection> intersections_set;
  for (const auto &i : iset) {
    intersections.push_back(*i);
    intersections_set.insert(*i);
  }
  return intersections_set.size() == intersections.size();
}

RC_BOOST_PROP(intersectSelf, (Myrectangle a)) {
  const unsigned n = *rc::gen::inRange(1, 10);
  std::vector<Myrectangle> rects{n, a};
  IntersectionSet iset{prectangles(rects)};
  // powerset (2^n) - individual rectangles (n) - empty set (1)
  auto expected = (1 << n) - n - 1;

  RC_ASSERT(iset.size() == expected);
  RC_ASSERT(is_unique(iset));
}

void check_iset(const std::vector<Myrectangle> &rects,
                const IntersectionSet &iset, unsigned expected) {
  bool size_ok = iset.size() == expected;
  bool is_unique_ok = is_unique(iset);
  if (size_ok && is_unique_ok) {
    return;
  }
  for (const auto r : rects) RC_LOG() << r << "\n";
  RC_LOG() << iset;
  RC_ASSERT(iset.size() == expected);
  RC_ASSERT(is_unique_ok);
}

RC_BOOST_PROP(intersectLinear, (Myrectangle a)) {
  RC_PRE(!(a.bot_right() == a.top_left));
  const unsigned n = *rc::gen::inRange(2, 10);
  std::vector<Myrectangle> rects;
  // 1 intersects 2, 2 intersects 3, but 1 doesn't intersect 3
  // ensured by the precondition above
  rects.push_back(a);
  for (unsigned i = 1; i < n; i++) {
    const Myrectangle &b = rects.back();
    rects.push_back(Myrectangle{b.bot_right(), b.size});
  }
  IntersectionSet iset{prectangles(rects)};
  check_iset(rects, iset, n - 1);
}

RC_BOOST_PROP(intersectOverlap, (Myrectangle a)) {
  RC_PRE(a.size.w % 2 == 1 && a.size.h % 2 == 1 && a.size.w > 1 &&
         a.size.h > 1);
  const unsigned n = *rc::gen::inRange(2, 10);
  std::vector<Myrectangle> rects;
  // rectangle 1 intersects 2&3; 2 intersects 3&4, ...
  // but 1 doesn't intersect 4
  // (1 and 3 intersect exactly in the corners)
  rects.push_back(a);
  for (unsigned i = 1; i < n; i++) {
    const Myrectangle &b = rects.back();
    Point bot{b.bot_right()};
    Point p{(bot.x + b.top_left.x) / 2, (bot.y + b.top_left.y) / 2};
    rects.push_back(Myrectangle{p, b.size});
  }
  IntersectionSet iset{prectangles(rects)};
  check_iset(rects, iset, 3 * (n - 2) + 1);
}

BOOST_AUTO_TEST_CASE(intersectOverlapLarge) {
  const unsigned n = 1000;

  Myrectangle a{Point{0, 0}, Size{3, 3}};
  std::vector<Myrectangle> rects;
  // rectangle 1 intersects 2&3; 2 intersects 3&4, ...
  // but 1 doesn't intersect 4
  // (1 and 3 intersect exactly in the corners)
  rects.push_back(a);
  for (unsigned i = 1; i < n; i++) {
    const Myrectangle &b = rects.back();
    Point bot{b.bot_right()};
    Point p{(bot.x + b.top_left.x) / 2, (bot.y + b.top_left.y) / 2};
    rects.push_back(Myrectangle{p, b.size});
  }
  IntersectionSet iset{prectangles(rects)};
  check_iset(rects, iset, 3 * (n - 2) + 1);
}

RC_BOOST_PROP(intersectAll, (Myrectangle a)) {
  auto rects = *rc::gen::container<std::vector<Myrectangle>>(
      10, rc::gen::arbitrary<Myrectangle>());
  IntersectionSet iset{prectangles(rects)};
  // contains all intersection sets possible between 2 or more rects
  std::set<Intersection> intersections;
  std::transform(iset.cbegin(), iset.cend(),
                 inserter(intersections, begin(intersections)),
                 [](const auto &pi) { return *pi; });
  std::set<Intersection> missing;
  for (const auto &i1 : intersections) {
    for (const auto &i2 : intersections) {
      Intersection i{i1, i2};
      if (!i.is_empty() && intersections.count(i) == 0) {
        missing.insert(i);
      }
    }
  }
  if (!missing.empty()) {
    for (const auto r : rects) {
      RC_LOG() << r << "\n";
    }
    for (const auto i : iset) {
      RC_LOG() << i << "\n";
    }
    RC_LOG() << "Missing:\n";
    for (const auto i : missing) {
      RC_LOG() << i << "\n";
    }
    RC_FAIL("has missing intersections");
  }
}

template <typename Stream>
std::vector<std::string> get_sorted_lines(Stream &is) {
  std::vector<std::string> out;
  std::string str;
  while (std::getline(is, str)) {
    out.push_back(str);
  }
  std::sort(std::begin(out), std::end(out));
  return out;
}

BOOST_AUTO_TEST_CASE(mainoutput) {
  auto argv = boost::unit_test::framework::master_test_suite().argv;
  auto argc = boost::unit_test::framework::master_test_suite().argc;
  BOOST_TEST(argc == 3);
  std::vector<char *> args(argv, argv + argc);

  std::ifstream expected_file(args.at(2));
  auto expected = get_sorted_lines(expected_file);

  std::stringstream ss;
  process(ss, args.at(1));

  std::string actual_s{ss.str()};
  std::istringstream actual_ss{actual_s};
  auto actual = get_sorted_lines(actual_ss);
  BOOST_TEST(actual == expected);

  std::stringstream ss2;
  process(ss2, args.at(1), 3);
  std::string actual_s2{ss2.str()};
  std::istringstream actual_ss2{actual_s2};
  auto actual2 = get_sorted_lines(actual_ss2);

  std::vector<std::string> expected2;
  std::copy_if(begin(expected), end(expected), std::back_inserter(expected2),
               [](const auto &s) {
                 return s.find("4:") == std::string::npos &&
                        s.find("4 ") == std::string::npos;
               });
  BOOST_TEST(actual2 == expected2);
}

BOOST_AUTO_TEST_CASE(invalid_json) {
  std::vector<std::string> tests{
      "[]",
      "{}",
      "{\"rects\":{}}",
      "{\"rects\":[{}]}",
      "{\"rects\":[[]]}",
      "{\"rects\":[{\"x\":1,\"y\":2,\"w\":3,\"h\":1.1}]}",
      "{\"rects\":[{\"x\":\"foo\",\"y\":2,\"w\":3,\"h\":1}]}",
      "{\"rects\":[{\"x\":1,\"y\":2,\"w\":3,\"h\":0}]}",
  };
  for (const auto &t : tests) {
    std::istringstream ss(t);
    BOOST_CHECK_THROW(parse_json(ss), std::logic_error);
  }

  std::vector<std::string> tests_ok{
      "{\"rects\":[{\"x\":1,\"y\":2,\"w\":3,\"h\":1}]}",
      "{\"rects\":[{\"x\":18014398509481984,\"y\":2,\"w\":3,\"h\":1}]}",
      "{\"rects\":[{\"x\":1,\"y\":2,\"w\":3,\"h\":1}], \"foo\":{}}",
      "{\"rects\":[{\"x\":1,\"y\":2,\"w\":3,\"h\":1,\"foo\":{}}]}",
  };
  for (const auto &t : tests_ok) {
    std::istringstream ss(t);
    BOOST_TEST(parse_json(ss).size() == 1u);
  }
}

}  // namespace
