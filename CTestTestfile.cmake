set(BUILD_DIR "obj")
function(ADD_CONFIG NAME CONFIG)
  add_test("${NAME}" ${wrapper} ctest -C "${CONFIG}"
    --build-and-test . "${BUILD_DIR}/${NAME}"
    --build-two-config --build-generator "Unix Makefiles" --build-noclean
    --test-command ${ARGN})
endfunction(ADD_CONFIG)

function(ADD_BUILD_TYPE CONFIG)
  ADD_CONFIG("${CONFIG}" "${CONFIG}"
    ctest -V -D ExperimentalTest -D "ExperimentalMemCheck")
endfunction(ADD_BUILD_TYPE)

# Build and run tests

# skip valgrind on debug builds: slow
ADD_CONFIG(Debug Debug ctest -V -D "ExperimentalTest")
ADD_BUILD_TYPE(RelWithDebInfo)
ADD_BUILD_TYPE(Release)

# Build and test source package
add_test(package_source sh -c "mkdir -p obj/cpack &&
 cd obj/cpack && cmake ../../ && make package_source &&
 unzip *.zip && cd *Source &&
 cmake . && make -j8 && make test")

# run coverage instead of memcheck
ADD_CONFIG(Coverage Coverage
  sh -c "./Intersecting\; ./Intersecting test/test.cpp\; ctest -V -D ExperimentalTest -D ExperimentalCoverage")

# Clang sanitizer build
find_program(CLANG "clang++")
if(CLANG)
  # Do not run memcheck in this case
  ADD_CONFIG(Sanitizer Sanitizer ctest -V)
  
  set_tests_properties(Sanitizer PROPERTIES
    ENVIRONMENT CXX=clang++
    ENVIRONMENT CC=clang
    FAIL_REGULAR_EXPRESSION "runtime error")
endif()

# Lint / Static analysis
find_program(CLANG_TIDY "clang-tidy")
if(CLANG_TIDY)
  add_test(ClangTidy sh -c "mkdir -p ${BUILD_DIR}/clang-tidy &&
          cd ${BUILD_DIR}/clang-tidy &&
          cmake ../../ && make clang-tidy")
  set_tests_properties(ClangTidy PROPERTIES
    FAIL_REGULAR_EXPRESSION " (warning|error):")
endif(CLANG_TIDY)

file(GLOB_RECURSE SOURCES "src/*.cpp" "src/*.hpp" "include/*.hpp"
  "test/*.cpp" "test/*.hpp")
find_program(CLANG_FORMAT "clang-format")
if(CLANG_FORMAT)
  add_test(ClangFormat "${CLANG_FORMAT}" -output-replacements-xml ${SOURCES})
  set_tests_properties(ClangFormat PROPERTIES
    FAIL_REGULAR_EXPRESSION "offset")
endif()

find_program(SCAN_BUILD "scan-build")
if(SCAN_BUILD)
  set(wrapper "${SCAN_BUILD}")
  ADD_CONFIG(ClangAnalyzer Debug)
  set_tests_properties(ClangAnalyzer PROPERTIES
    PASS_REGULAR_EXPRESSION "No bugs found")
endif()

# TODO: generate docs
